Run these commands in order in TTY or Terminal.
```
wget -qO - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
```

```
echo "### THIS FILE IS AUTOMATICALLY CONFIGURED ###
# You may comment out this entry, but any other modifications may be lost.
deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" | sudo tee -a /etc/apt/sources.list.d/google-chrome.list
```

```
sudo apt update && sudo apt install google-chrome-stable
```
