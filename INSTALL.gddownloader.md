Run these commands in TTY shell or a Terminal
```
sudo download https://gist.github.com/Austcool-Walker/d7b205cd6a17022d415d496e93155483/raw/c3da6c707965e3137e0aa017fa4cbeb1e6178509/gddownloader.py /usr/bin/gddownloader && sudo chmod -v +x /usr/bin/gddownloader
```
Or
```
sudo curl -o  /usr/bin/gddownloader https://gist.github.com/Austcool-Walker/d7b205cd6a17022d415d496e93155483/raw/c3da6c707965e3137e0aa017fa4cbeb1e6178509/gddownloader.py && sudo chmod -v +x /usr/bin/gddownloader
```

```
sudo pip3 install git+https://github.com/ndrplz/google-drive-downloader.git
```
