#!/bin/bash
# Build Bash from sources on Ubuntu.

# Make script gives up on any error
set -e

# Some packages may be missing
sudo apt-get install -y git-core gcc make autoconf yodl libncursesw5-dev texinfo checkinstall

# Clone zsh repo and change to it
# git clone https://git.savannah.gnu.org/git/bash.git bash
cd bash

# Get lastest stable version, but you can change to any valid branch/tag/commit id
BRANCH=$(git describe --abbrev=0 --tags)
# Get version number, and revision/commit id when this is available
BASH_VERSION=$(echo $BRANCH | cut -d '-' -f2,3,4)
# Go to desired branch
git checkout $BRANCH


# Updated to bash 5.0.2 on Trusty Tahr (pre-release)
./configure --prefix=/usr \
			--enable-largefile \
			--infodir=/usr/share/info \
			--mandir=/usr/share/man \
			--without-bash-malloc \
			--with-installed-readline \
			--enable-static-link \
            LDFLAGS="-Wl,--as-needed -g -Wl,-Bsymbolic-functions -Wl,-z,relro"

# Compile, test and install
make -j18
sudo checkinstall -y --pkgname=bash --pkgversion=$BASH_VERSION --pkglicense=GPL3

# Make zsh the default shell
# sudo sh -c "echo /bin/bash >> /etc/shells"
# chsh -s /bin/bash