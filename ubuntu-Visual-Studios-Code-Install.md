Run these commands in order in TTY or Terminal.

```
sudo sh -c 'echo "deb [arch=amd64 trusted=yes allow-insecure=yes] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list' && sudo apt update
```

```
sudo apt install code
```
