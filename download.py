#!/usr/bin/python3
import requests
from clint.textui import progress
import sys

url = sys.argv[1]
path = sys.argv[2]
#url = 'https://phoenixnap.dl.sourceforge.net/project/macbuntuos-16-04/18.04.3-2020.02.24/macbuntu-18.04.3-2020.02.24-unity-amd64.iso'
    #target_path = 'macbuntu-18.04.3-2020.02.24-unity-amd64.iso'

r = requests.get(url, stream=True)
#path = 'macbuntu-18.04.3-2020.02.24-unity-amd64.iso'
with open(path, 'wb') as f:
        total_length = int(r.headers.get('content-length'))
        for chunk in progress.bar(r.iter_content(chunk_size=1024), expected_size=(total_length/1024) + 1): 
            if chunk:
                f.write(chunk)
                f.flush()