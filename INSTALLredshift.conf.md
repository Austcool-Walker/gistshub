Run these commands in TTY or Terminal

```
mkdir -p ~/.config/redshift && curl https://gist.github.com/Austcool-Walker/554207da8db202f0ba712fbdcd5fb573/raw/fb941c300aeb01236d9eaab408030a64a7490a17/redshift.conf --progress-bar -L -o ~/.config/redshift/redshift.conf && sudo chmod -v +x ~/.config/redshift/redshift.conf
```