Run these commands in order in TTY or Terminal.
```
wget -qO - https://assets.checkra.in/debian/archive.key | sudo apt-key add -
```

```
echo "deb https://assets.checkra.in/debian /" | sudo tee -a /etc/apt/sources.list.d/checkra1n.list && sudo apt update
```

```
sudo apt install checkra1n
```
