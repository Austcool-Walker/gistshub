#!/bin/bash
# See http://blog.coolaj86.com/articles/google-repos-for-linux.html
# See Also https://gist.github.com/4686265 for the repo list files

wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -

pushd /etc/apt/sources.list.d/
wget -c https://gist.github.com/raw/4686265/google-chrome.list
wget -c https://gist.github.com/raw/4686265/google-musicmanager.list
wget -c https://gist.github.com/raw/4686265/google-earth.list
wget -c https://gist.github.com/raw/4686265/google-talkplugin.list
wget -c https://gist.github.com/raw/4686265/mod-pagespeed.list
popd

apt-get update