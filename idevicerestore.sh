
OUT=/opt/libimobiledevice.git

sudo apt update
# sudo apt upgrade
sudo apt install -y libcurl4-openssl-dev libplist-dev libzip-dev openssl libssl-dev libusb-1.0-0-dev libreadline-dev build-essential git make automake libtool pkg-config
git clone https://github.com/libimobiledevice/libirecovery
git clone https://github.com/libimobiledevice/idevicerestore
git clone https://github.com/libimobiledevice/usbmuxd
git clone https://github.com/libimobiledevice/libimobiledevice
git clone https://github.com/libimobiledevice/libusbmuxd
git clone https://github.com/libimobiledevice/libplist
git clone https://github.com/libimobiledevice/ifuse

sudo mkdir -p /opt/libimobiledevice.git
cd libplist && ./autogen.sh --without-cython && sudo make DESTDIR=$OUT install && cd ..
cd libusbmuxd && ./autogen.sh && sudo make DESTDIR=$OUT install && cd ..
cd libimobiledevice && ./autogen.sh --without-cython && sudo make DESTDIR=$OUT install && cd ..
cd usbmuxd && ./autogen.sh && sudo make DESTDIR=$OUT install && cd ..
cd libirecovery && ./autogen.sh && sudo make DESTDIR=$OUT install && cd ..
cd idevicerestore && ./autogen.sh && sudo make DESTDIR=$OUT install && cd ..
cd ifuse && ./autogen.sh && sudo make DESTDIR=$OUT install && cd ..
sudo ldconfigs

cd  /opt/libimobiledevice.git/
sudo bsdtar -vcJf ~/libimobiledevice.git.txz ./*