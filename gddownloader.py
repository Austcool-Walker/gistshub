#!/opt/local/bin/python3
import requests
from clint.textui import progress
import sys

file_id = sys.argv[1]
dest_path = sys.argv[2]

from google_drive_downloader import GoogleDriveDownloader as gdd

gdd.download_file_from_google_drive(file_id,
                                    dest_path,
                                    unzip=True)
