```bash
sudo wget -O /usr/bin/download "https://gist.githubusercontent.com/Austcool-Walker/a22a687cebb3d6bbc1a7cd40b769caea/raw/15bb627a54005117d7c2b5c088cc6dad5449e86b/download.py" && sudo chmod -v +x /usr/bin/download
```
or on certain systems you can run.

```bash
sudo download "https://gist.githubusercontent.com/Austcool-Walker/a22a687cebb3d6bbc1a7cd40b769caea/raw/15bb627a54005117d7c2b5c088cc6dad5449e86b/download.py" /bin/download && sudo chmod -v +x /bin/download
```

```bash
sudo curl -Ls -o /bin/download "https://gist.githubusercontent.com/Austcool-Walker/a22a687cebb3d6bbc1a7cd40b769caea/raw/15bb627a54005117d7c2b5c088cc6dad5449e86b/download.py" && sudo chmod -v +x /bin/download
```