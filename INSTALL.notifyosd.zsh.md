Run these commands in TTY shell or a Terminal
```
sudo download https://gist.github.com/Austcool-Walker/03a8838efaeb8237304f6f032d4bef50/raw/d446ba26cc6ca9f1f4557f907ca5c7aa26c27078/notifyosd.zsh /usr/local/share/zsh/site-functions/notifyosd.zsh
```
Or
```
sudo curl -o /usr/local/share/zsh/site-functions/notifyosd.zsh https://gist.github.com/Austcool-Walker/03a8838efaeb8237304f6f032d4bef50/raw/d446ba26cc6ca9f1f4557f907ca5c7aa26c27078/notifyosd.zsh
```

```
touch ~/.zprofile
```

```
sudo chmod -v +x ~/.zprofile && sudo chmod -v +x /usr/local/share/zsh/site-functions/notifyosd.zsh
```

```
echo "
# zprofile and undistractme zsh
source ~/.zprofile
source /usr/local/share/zsh/site-functions/notifyosd.zsh" | >> ~/.zshrc
```

```
sudo apt install libnotify-bin sox
```

