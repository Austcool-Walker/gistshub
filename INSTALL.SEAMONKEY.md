Install seamonkey browser ubuntu

Run these commands in Terminal or TTY.
```
cat <<EOF | sudo tee /etc/apt/sources.list.d/mozilla.list
deb http://downloads.sourceforge.net/project/ubuntuzilla/mozilla/apt all main
EOF
```
```
sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 2667CA5C
```
```
sudo apt-get update && sudo apt-get install seamonkey-mozilla-build
```
