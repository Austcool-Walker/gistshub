# Install BetterDiscord on Linux

This Gist contains simple instructions on how to install,
update, and uninstall BetterDiscord on Linux.

For more thorough documentation,
take a look at `betterdiscordctl`'s [README](https://github.com/bb010g/betterdiscordctl#betterdiscordctl).

**Do NOT submit issues here as I don't check the comments.
You should submit them [here](https://github.com/bb010g/betterdiscordctl/issues) instead.**

## Install dependencies

### Git

Install using your [package manager](https://git-scm.com/download/linux)

## Install betterdiscordctl

```
sudo curl https://raw.githubusercontent.com/bb010g/betterdiscordctl/master/betterdiscordctl --progress-bar -L -o /usr/local/bin/betterdiscordctl && sudo chmod -v +x /usr/local/bin/betterdiscordctl
```

> You can then keep `betterdiscordctl` up to date with this command:
> ```
> $ sudo betterdiscordctl upgrade
> ```

## Install BetterDiscord

Replace `[COMMAND]` with `install` to install BD for the first time,
`update` to update BD to the latest version,
`reinstall` to reinstall BD after a Discord update,
or `uninstall` to uninstall an existing installation.

- For Stable

```
$ betterdiscordctl [COMMAND]
```

- For PTB

```
$ betterdiscordctl [COMMAND] -f PTB
```

- For Canary

```
$ betterdiscordctl [COMMAND] -f Canary
```

- For Snap

```
$ betterdiscordctl [COMMAND] --snap
```

- For Flatpak

```
$ betterdiscordctl [COMMAND] --flatpak
```

## Common Issues

`Discord intallation not found`

Point `betterdiscordctl` to the correct path with the `--scan` flag.

`Discord modules directory not found` or `Config directory not found`

Discord has to be started at least once to create
the necessary directories before using `betterdiscordctl`.

`git : command not found`

You don't have `git` installed. Install it and try again.

## Credits

#### [BetterDiscord](https://github.com/Jiiks/BetterDiscordApp) by Jiiks

#### [Bandaged BD](https://github.com/rauenzi/BetterDiscordApp) by Zerebos (rauenzi)

#### [betterdiscordctl](https://github.com/bb010g/betterdiscordctl) by bb010g