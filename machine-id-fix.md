Run these commands in TTY or Terminal.

```
sudo su && sudo passwd
```

and type in a new password for root.

```
exit && su
```

```
rm -f /etc/machine-id && rm /var/lib/dbus/machine-id && dbus-uuidgen --ensure=/etc/machine-id && dbus-uuidgen --ensure && exit
```
